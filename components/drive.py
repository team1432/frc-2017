""" Drive is easy interaction with the drivetrain """

import wpilib

from helpers import *
from magicbot.magic_tunable import tunable
from common import ir

# Constants are variables that won't change.
# They belong here


class Drive:
    """
        The sole interaction between the robot and its driving system
        occurs here. Anything that wants to drive the robot must go
        through this class.
    """

    robot_drive = wpilib.RobotDrive

    auto_rotate_speed = tunable(1.0)
    auto_drive_speed = tunable(1.0)

    is_auto_driving = False

    left_drive_motor = wpilib.Spark
    right_drive_motor = wpilib.Spark

    rotation_amount = 0
    forward_amount = 0

    squared_inputs = False
    backwards = False

    def __init__(self):
        pass

    # Info functions -- these give information to other methods

    # Verb functions -- these functions do NOT talk to motors directly. This
    # allows multiple callers in the loop to call our functions without
    # conflicts.

    def forwards_at(self, speed):
        """Causes the robot to drive forwards at the given speed"""
        self.forward_amount = speed

    def turn_at(self, rotation_speed):
        """Causes the robot to turn at the given speed"""
        self.rotation_amount = rotation_speed

    def set_direction(self, direction):
        """Used to reverse direction"""
        self.backwards = bool(direction)

    def switch_direction(self):
        """when called the robot will reverse front/back"""
        self.backwards = not self.backwards

    # Execute function -- This is called automatically at the end of
    # teleopPeriodic / autonomousPeriodic. This directly interacts with
    # WPILib.

    def execute(self):
        """Actually makes the robot drive"""

        self.robot_drive.arcadeDrive(
            self.forward_amount,
            self.rotation_amount,
            self.squared_inputs)

        self.forward_amount = 0
        self.rotation_amount = 0
