"""
This is an autonomous module for driving to the side lift using
vision processing
"""

from components import drive
from components import camera
from robotpy_ext.autonomous import timed_state, StatefulAutonomous
import wpilib
from magicbot.magic_tunable import tunable
from networktables import NetworkTables

root_nt = NetworkTables.getTable('/')


class sideLiftVision(StatefulAutonomous):
    """
    Drives the robot to the side lift using vision processing
    """

    MODE_NAME = "Side Lift (Vision)"
    DEFAULT = False

    drive = drive.Drive
    camera = camera.Camera
    side = tunable('left')

    @timed_state(duration=1.5, first=True, next_state='start_turn')
    def forward(self):
        """Drives the majority of the distance to the lift"""
        self.drive.forwards_at(0.5)

    @timed_state(duration=0.7, next_state='turn')
    def start_turn(self, initial_call):
        if initial_call:
            print('first')
            self.camera.start_processing()

        side = root_nt.getString('/autonomous/Side Lift (Vision)/side', 'left')
        print(side)
        if side == 'left':
            goal_x = -0.5
        else:
            goal_x = 0.5

        self.drive.turn_at(goal_x)

    @timed_state(duration=1.8, next_state='boost')
    def turn(self):
        """turn to lift with camera"""

        turn_val = self.camera.get_x() * 1.4

        print(turn_val)

        self.drive.turn_at(turn_val)

    @timed_state(duration=1.0, next_state='slow')
    def boost(self, initial_call):
        self.drive.forwards_at(0.4)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)

    @timed_state(duration=8.0)
    def slow(self, initial_call):
        """Drives the remainder of the distance to the lift slowly"""
        self.drive.forwards_at(0.35)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)
