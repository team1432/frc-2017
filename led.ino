int r1 = 6;
int r2 = 10;
int g1 = 3;
int g2 = 9;
int b1 = 5;
int b2 = 11;

int input0 = 4;
int input1 = 8;
int input_value=0;


int outputs[] ={6,3,5,10,9,11};
int outputcount =6;

int rgb_values[] = {0,0,0,0,0,0};
const int Red = 0;
const int Green = 1;
const int Blue = 2;

int fireincreasing =0;

int currentColors[] = {0, 0, 0};
int goalColors[] = {0, 0, 0};

long loopCount = 0;

void setup() {
  // put your setup code here, to run once:
  for(int i = 0;i<outputcount;i++)
  {
    pinMode(outputs[i],OUTPUT);
    digitalWrite(outputs[i],LOW);
  }
  pinMode(input0,INPUT);
  pinMode(input1,INPUT);
  Serial.begin(9600);
}
void writeledsoff()
{
   for(int i = 0;i<outputcount;i++)
  {
      digitalWrite(outputs[i],LOW);
  }
}
void updateFireLED1()
{
  //LED Red 1 
  rgb_values[Red] = 255;
  if(fireincreasing ==0)
  {
    rgb_values[Green] -=1;
    if(rgb_values[Green] <=20)
      fireincreasing =1;
  }
  else
  {
    rgb_values[Green]+=1;
    if(rgb_values[Green] >= 70)
       fireincreasing =0;
  } 
  rgb_values[Blue] = 0;
}
void writeoutputs()
{
  for(int i=0;i<outputcount;i++)
  {
    analogWrite(outputs[i],rgb_values[i]);
  }
}
int readinputs()
{
  
  int input0_val = digitalRead(input0);
  int input1_val = digitalRead(input1);
  return input0_val|(input1_val<<1);
  
}
void print(int string) {
  Serial.print("\r\n" + string);
}

void updateOutputs() {
  for (int i=0; i<3; i++) {
    int currentColor = currentColors[i];
    int goalColor = goalColors[i];

    if (currentColor < goalColor) {
      currentColors[i]++;
    } else if (currentColor > goalColor) {
      currentColors[i]--;
    }
    analogWrite(outputs[i], currentColors[i]);
  }
}

void setColors(int r, int g, int b) {
  goalColors[Red] = r;
  goalColors[Green] = g;
  goalColors[Blue] = b;
}

void loop() {
  switch(readinputs())
  {
    case 0:
      setColors(0, 0, 255);
      delay(1);
    break;
    case 1:
      setColors(255, 0, 0);
      delay(1);
    break;
    case 2:
      setColors(255,255,0);
    break;
    case 3:
      setColors(0,255,0);
    break;
  }
  
  /*loopCount++;
  delay(5);
  if (loopCount == 1000) {
    loopCount = 0;
    setColors(255, 0, 0);
  }
  if (loopCount == 500) {
    setColors(255, 80, 0);
  }
  */
  //int value = readinputs();
  // writeoutputs();
  // updateFireLED1();
  updateOutputs();
  //delay(100);
  //Serial.print(readinputs());
  //Serial.write("\r\n");
}
