""" Physics is the module where robot parts are described for the simulator """
from pyfrc.physics.drivetrains import two_motor_drivetrain


class PhysicsEngine:
    """ This is the engine which runs with the sim """

    def __init__(self, controller):
        self.controller = controller

    """
        Update pyfrc simulator
        Keyword arguments:
        self -- Global dictionary of everything.
        now -- Current time in ms
        tm_diff -- Diff between current time and time when last checked
    """

    def update_sim(self, hal_data, now, tm_diff):
        """ Updates the simulation with new robot positions """

        left_value = hal_data['pwm'][0]['value']
        right_value = -hal_data['pwm'][1]['value']

        rcw, fwd = two_motor_drivetrain(left_value, right_value, speed=6)

        self.controller.drive(-fwd, rcw, tm_diff)
